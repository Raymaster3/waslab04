{
    "created_at": "Thu Mar 04 09:41:59 +0000 2021",
    "id": 1367409982715686912,
    "id_str": "1367409982715686912",
    "text": "RT @cfarre: Beyond REST by Netflix Technology Blog https://t.co/rmvjbg6phj",
    "truncated": false,
    "entities": {
        "hashtags": [],
        "symbols": [],
        "user_mentions": [
            {
                "screen_name": "cfarre",
                "name": "carleѕ ғarré",
                "id": 18244676,
                "id_str": "18244676",
                "indices": [
                    3,
                    10
                ]
            }
        ],
        "urls": [
            {
                "url": "https://t.co/rmvjbg6phj",
                "expanded_url": "https://netflixtechblog.com/beyond-rest-1b76f7c20ef6",
                "display_url": "netflixtechblog.com/beyond-rest-1b…",
                "indices": [
                    51,
                    74
                ]
            }
        ]
    },
    "source": "",
    "in_reply_to_status_id": null,
    "in_reply_to_status_id_str": null,
    "in_reply_to_user_id": null,
    "in_reply_to_user_id_str": null,
    "in_reply_to_screen_name": null,
    "user": {
        "id": 912751218304409600,
        "id_str": "912751218304409600",
        "name": "Raymaster",
        "screen_name": "Raymaster03",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 10,
        "friends_count": 125,
        "listed_count": 0,
        "created_at": "Tue Sep 26 18:50:23 +0000 2017",
        "favourites_count": 16263,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 268,
        "lang": null,
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/1232302212442923010/zV3Kcd4e_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/1232302212442923010/zV3Kcd4e_normal.jpg",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": true,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
    },
    "geo": null,
    "coordinates": null,
    "place": null,
    "contributors": null,
    "retweeted_status": {
        "created_at": "Thu Mar 04 08:33:27 +0000 2021",
        "id": 1367392733669842945,
        "id_str": "1367392733669842945",
        "text": "Beyond REST by Netflix Technology Blog https://t.co/rmvjbg6phj",
        "truncated": false,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [],
            "urls": [
                {
                    "url": "https://t.co/rmvjbg6phj",
                    "expanded_url": "https://netflixtechblog.com/beyond-rest-1b76f7c20ef6",
                    "display_url": "netflixtechblog.com/beyond-rest-1b…",
                    "indices": [
                        39,
                        62
                    ]
                }
            ]
        },
        "source": "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 18244676,
            "id_str": "18244676",
            "name": "carleѕ ғarré",
            "screen_name": "cfarre",
            "location": "41.38846, 2.11383",
            "description": "Tenured Assistant Professor @la_UPC. Member of @gessi_upc.  Like it or not, we live in interesting times.",
            "url": "https://t.co/vTK5ecmPoS",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "https://t.co/vTK5ecmPoS",
                            "expanded_url": "http://www.essi.upc.edu/~farre/",
                            "display_url": "essi.upc.edu/~farre/",
                            "indices": [
                                0,
                                23
                            ]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 291,
            "friends_count": 330,
            "listed_count": 29,
            "created_at": "Fri Dec 19 16:18:57 +0000 2008",
            "favourites_count": 680,
            "utc_offset": null,
            "time_zone": null,
            "geo_enabled": true,
            "verified": false,
            "statuses_count": 102,
            "lang": null,
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": false,
            "profile_background_color": "C0DEED",
            "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
            "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
            "profile_background_tile": false,
            "profile_image_url": "http://pbs.twimg.com/profile_images/942466110955442176/tdx4XCfM_normal.jpg",
            "profile_image_url_https": "https://pbs.twimg.com/profile_images/942466110955442176/tdx4XCfM_normal.jpg",
            "profile_banner_url": "https://pbs.twimg.com/profile_banners/18244676/1581857499",
            "profile_link_color": "1B95E0",
            "profile_sidebar_border_color": "000000",
            "profile_sidebar_fill_color": "000000",
            "profile_text_color": "000000",
            "profile_use_background_image": true,
            "has_extended_profile": false,
            "default_profile": false,
            "default_profile_image": false,
            "following": false,
            "follow_request_sent": false,
            "notifications": false,
            "translator_type": "regular"
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "is_quote_status": false,
        "retweet_count": 3,
        "favorite_count": 0,
        "favorited": false,
        "retweeted": true,
        "possibly_sensitive": false,
        "lang": "en"
    },
    "is_quote_status": false,
    "retweet_count": 3,
    "favorite_count": 0,
    "favorited": false,
    "retweeted": true,
    "possibly_sensitive": false,
    "lang": "en"
}
