package twitter;

import java.util.Date;

import twitter4j.FilterQuery;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterBase;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;

public class SimpleClient {
	public static void main(String[] args) throws Exception {
		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		FilterQuery fq = new FilterQuery();

        String keywords[] = {"#covid19"};

        fq.track(keywords);
        
        StatusListener listener = new StatusListener() {

            @Override
            public void onException(Exception arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onScrubGeo(long arg0, long arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStatus(Status status) {
                User user = status.getUser();
                
                // gets Username
                String Name = user.getName();
                String username = status.getUser().getScreenName();
                String content = status.getText();
                System.out.println(Name + " " + "(@" + username + "): " + content +"\n");
            }

            @Override
            public void onTrackLimitationNotice(int arg0) {
                // TODO Auto-generated method stub

            }

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

        };

        twitterStream.addListener(listener);
        twitterStream.filter(fq);    
	}
}
